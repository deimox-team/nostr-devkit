import { SimplePool } from 'nostr-tools';
import { RelayType } from './types/RelayType';
import { ProfileType } from './types/UserType';



/**
 * Nostr tools DevKit
 */
export default class NTDK {
  private _pool: SimplePool
  private _relays: RelayType['address'][]
  constructor(relays: RelayType['address'][]) {
    this._pool = new SimplePool()
    this._relays = relays
  }

  /**
   * Aggregates results for all profile related data
   * 
   * @param pubKey bech32 public key associated with a user
   * @returns 
   */
  public async findProfileData(pubKey: string): Promise<ProfileType> {
    return {
      metadata: await this.findProfileMetadata(pubKey),
      follows: await this.findFollows(pubKey),
      followers: await this.findFollowers(pubKey)
    }
  }

  /**
   * Finds profile metadata
   * 
   * @param pubKey bech32 public key associated with a user
   * @returns 
   */
  public async findProfileMetadata(pubKey: string) {
    const metaEvents = await this._pool.list(this._relays, [{ kinds: [0], authors: [pubKey] }])
    let metadata: ProfileType['metadata'] = {}
    if (metaEvents[0] && metaEvents[0].content) {
      metadata = JSON.parse(metaEvents[0].content)
    }

    return metadata;
  }

  /**
   * Gets follows
   * 
   * @param pubKey 
   * @returns 
   */
  public async findFollows(pubKey: string): Promise<ProfileType['follows']> {
    const contactEvents = await this._pool.list(this._relays, [{ kinds: [3], authors: [pubKey] }])
    
    return contactEvents[0] ? contactEvents[0].tags
    .filter(t => t[0] === 'p')
    .map(t => t[1])
    : []
  }
  
  /**
   * Gets followers
   * 
   * @param pubKey 
   * @returns 
   */
  public async findFollowers(pubKey: string): Promise<ProfileType['followers']> {
    return (await this._pool.list(this._relays, [{ kinds: [3], '#p': [pubKey] }])).map(e => e.pubkey)
  }

  public async findUserNotes(pubKey: string) {
    return await this._pool.list(this._relays, [{ kinds: [1], authors: [pubKey] }])
  }
}