import { RelayType } from './types/RelayType';
import { ProfileType } from './types/UserType';
/**
 * Nostr tools DevKit
 */
export default class NTDK {
    private _pool;
    private _relays;
    constructor(relays: RelayType['address'][]);
    /**
     * Aggregates results for all profile related data
     *
     * @param pubKey bech32 public key associated with a user
     * @returns
     */
    findProfileData(pubKey: string): Promise<ProfileType>;
    /**
     * Finds profile metadata
     *
     * @param pubKey bech32 public key associated with a user
     * @returns
     */
    findProfileMetadata(pubKey: string): Promise<import("./types/UserType").ProfileMetadata>;
    /**
     * Gets follows
     *
     * @param pubKey
     * @returns
     */
    findFollows(pubKey: string): Promise<ProfileType['follows']>;
    /**
     * Gets followers
     *
     * @param pubKey
     * @returns
     */
    findFollowers(pubKey: string): Promise<ProfileType['followers']>;
    findUserNotes(pubKey: string): Promise<import("nostr-tools").Event<1>[]>;
}
//# sourceMappingURL=index.d.ts.map