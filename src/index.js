"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var nostr_tools_1 = require("nostr-tools");
/**
 * Nostr tools DevKit
 */
var NTDK = /** @class */ (function () {
    function NTDK(relays) {
        this._pool = new nostr_tools_1.SimplePool();
        this._relays = relays;
    }
    /**
     * Aggregates results for all profile related data
     *
     * @param pubKey bech32 public key associated with a user
     * @returns
     */
    NTDK.prototype.findProfileData = function (pubKey) {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = {};
                        return [4 /*yield*/, this.findProfileMetadata(pubKey)];
                    case 1:
                        _a.metadata = _b.sent();
                        return [4 /*yield*/, this.findFollows(pubKey)];
                    case 2:
                        _a.follows = _b.sent();
                        return [4 /*yield*/, this.findFollowers(pubKey)];
                    case 3: return [2 /*return*/, (_a.followers = _b.sent(),
                            _a)];
                }
            });
        });
    };
    /**
     * Finds profile metadata
     *
     * @param pubKey bech32 public key associated with a user
     * @returns
     */
    NTDK.prototype.findProfileMetadata = function (pubKey) {
        return __awaiter(this, void 0, void 0, function () {
            var metaEvents, metadata;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._pool.list(this._relays, [{ kinds: [0], authors: [pubKey] }])];
                    case 1:
                        metaEvents = _a.sent();
                        metadata = {};
                        if (metaEvents[0] && metaEvents[0].content) {
                            metadata = JSON.parse(metaEvents[0].content);
                        }
                        return [2 /*return*/, metadata];
                }
            });
        });
    };
    /**
     * Gets follows
     *
     * @param pubKey
     * @returns
     */
    NTDK.prototype.findFollows = function (pubKey) {
        return __awaiter(this, void 0, void 0, function () {
            var contactEvents;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._pool.list(this._relays, [{ kinds: [3], authors: [pubKey] }])];
                    case 1:
                        contactEvents = _a.sent();
                        return [2 /*return*/, contactEvents[0] ? contactEvents[0].tags
                                .filter(function (t) { return t[0] === 'p'; })
                                .map(function (t) { return t[1]; })
                                : []];
                }
            });
        });
    };
    /**
     * Gets followers
     *
     * @param pubKey
     * @returns
     */
    NTDK.prototype.findFollowers = function (pubKey) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._pool.list(this._relays, [{ kinds: [3], '#p': [pubKey] }])];
                    case 1: return [2 /*return*/, (_a.sent()).map(function (e) { return e.pubkey; })];
                }
            });
        });
    };
    NTDK.prototype.findUserNotes = function (pubKey) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._pool.list(this._relays, [{ kinds: [1], authors: [pubKey] }])];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return NTDK;
}());
exports.default = NTDK;
//# sourceMappingURL=index.js.map