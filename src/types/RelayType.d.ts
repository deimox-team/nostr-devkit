export interface RelayType {
    address: string;
    connected: boolean;
    pub: boolean;
    sub: boolean;
}
//# sourceMappingURL=RelayType.d.ts.map