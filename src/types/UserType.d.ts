export interface UserType {
    npub: string;
}
export interface ProfileType {
    /**
     * Metadata related to the npub in question
     */
    metadata: ProfileMetadata;
    /**
     * Array of keys (public bech32) of people the npub in question added to their contact list
     */
    follows: string[];
    /**
     * Array of keys (public bech32) of people that have the npub in question in their contact list.
     */
    followers: string[];
}
export interface ProfileMetadata {
    /**
     * Also known as bio or motto
     */
    about?: string;
    /**
     * TBS
     */
    banner?: string;
    /**
     * TBS
     */
    display_name?: string;
    /**
     * Lightning Network address
     */
    lud16?: string;
    /**
     * Also known as handle. Usually displayed as @handle
     */
    name?: string;
    /**
     * Verification of domain ownership . Usually displayed as user@domain.tld
     */
    nip05?: string;
    /**
     * Avatar/profile picture
     */
    picture?: string;
}
//# sourceMappingURL=UserType.d.ts.map