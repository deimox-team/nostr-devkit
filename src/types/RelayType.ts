export interface RelayType {
  address: string
  connected: boolean
  pub: boolean
  sub: boolean
}